
#from django import forms

from django import forms
from .models import SNP, DISEASE_TRAIT
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
	first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class ConnexionForm(forms.Form):

	username = forms.CharField(label="Nom d'utilisateur", max_length=30)

	password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)


class SnpForm(forms.Form):

	Chromosome = forms.IntegerField(min_value=1, label="N° Chromosome", widget=forms.NumberInput( attrs={'size':'10'}))

	Chromosome_pos = forms.IntegerField(min_value=1, label="Chromosome position",required = False, widget=forms.NumberInput( attrs={'size':'10'}))

	Rsid = forms.IntegerField(min_value=1, label="Reference SNP Custer",required = False, widget=forms.NumberInput( attrs={'size':'10'}))

class PhenoList(forms.Form):

	CHOICES = []
	phenolist = DISEASE_TRAIT.objects.all()
	for p in phenolist:
	
		CHOICES.append((p.id,p.Name))

	PhenoChoice = forms.CharField(label="Disease", max_length=500, widget=forms.Select(choices= CHOICES))




		


