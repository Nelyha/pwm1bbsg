# Generated by Django 2.0 on 2018-05-12 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ProjetApp', '0014_disease_trait_reference_snp_snp2phenotype2ref'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reference',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='snp',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='snp2phenotype2ref',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
