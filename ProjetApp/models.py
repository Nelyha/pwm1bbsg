from django.db import models

# Create your models here.




class SNP(models.Model): 

	Chrom = models.IntegerField()
	Chrom_pos = models.IntegerField()
	Rsid = models.IntegerField()


class Reference(models.Model):


	Pubmedid = models.IntegerField()
	Journal = models.CharField(max_length=500) 
	Title = models.CharField(max_length=500) 
	Date = models.DateField()
	Link = models.CharField(max_length=500)

class DISEASE_TRAIT(models.Model):


	Name = models.CharField(max_length=500)



class SNP2Phenotype2Ref(models.Model):


	Snp = models.ForeignKey(SNP, on_delete=models.CASCADE)
	Disease_trait = models.ForeignKey(DISEASE_TRAIT, on_delete=models.CASCADE, )
	Reference = models.ForeignKey(Reference, on_delete=models.CASCADE)
	pvalue = models.FloatField()
	neglog10pvalue = models.FloatField()

