

from django.shortcuts import render, render_to_response, redirect
from .models import SNP , Reference, DISEASE_TRAIT, SNP2Phenotype2Ref
from django.http import HttpResponse, HttpResponseRedirect
from .forms import SnpForm, ConnexionForm, SignUpForm, PhenoList
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout 
from django.contrib.auth.decorators import login_required





def sign_up(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'T/signup.html', {'form': form})

def Home(request,):
    return render(request, "T/home.html")



@login_required(login_url='/connexion/')
def Snp_search(request):

	form = SnpForm(request.POST or None)
	if request.method == 'POST' :
		form = SnpForm(request.POST or None)

		if form.is_valid(): 

			chrom = form.cleaned_data['Chromosome']

			chrom_p = form.cleaned_data['Chromosome_pos']

			rsid = form.cleaned_data['Rsid']
			#snps = ""
			if chrom_p == None and rsid == None:
				snps = SNP.objects.filter(Chrom = chrom)
			
			if chrom_p != None  and rsid == None :
				snps = SNP.objects.filter(Chrom = chrom, Chrom_pos = chrom_p)

			if chrom_p == None and rsid != None :
				snps = SNP.objects.filter(Chrom = chrom, Rsid = rsid)								
	


			return render(request, 'T/result.html', {'snps': snps,})

	return render(request, 'T/SF.html', locals())

@login_required(login_url='/connexion/')
def Redirection_details(request, pk):

	sp = SNP.objects.filter(id=pk)

	pheno = DISEASE_TRAIT.objects.filter(snp2phenotype2ref__Snp_id=pk)[0]

	pv = SNP2Phenotype2Ref.objects.filter(Disease_trait=pheno.id)[0]

	study = Reference.objects.filter(snp2phenotype2ref__Disease_trait=pheno.id)[0]


	return render(request, 'T/Redirection_detail.html', {'sp': sp, 'pheno': pheno, 'pv':pv , 'st':study})


@login_required(login_url='/connexion/')
def PhenoDetails(request, pk):

	pheno = DISEASE_TRAIT.objects.filter(id=pk)[0]
	resultat = []

	snp = SNP2Phenotype2Ref.objects.filter(Disease_trait__id=pk)

	Nsnp = len(snp) 

	Ref = Reference.objects.filter(snp2phenotype2ref__Disease_trait = pk)

	s2p2r = SNP2Phenotype2Ref.objects.filter(Disease_trait = pk)
	
	for s, r, p in zip(snp, Ref, s2p2r):
		
		resultat.append((s,r,p))


	return render(request, 'T/PhenoDetails.html', { 'pheno': pheno, 'snp': snp, 'Nsnp' : Nsnp, 'Ref' : Ref, 'resultat': resultat })

@login_required(login_url='/connexion/')
def Phenotype(request):

	form = PhenoList(request.POST or None)
	if request.method == 'POST' :
		form = PhenoList(request.POST or None)


		if form.is_valid(): 
	
			PhenoChoice = form.cleaned_data['PhenoChoice']



			return redirect('/Phenotype_detail_page/'+PhenoChoice+'/')

		
	pheno = DISEASE_TRAIT.objects.all()

	return render(request, 'T/phenoL.html', locals())





