"""PrgWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from ProjetApp.views import Home , sign_up, PhenoDetails, Phenotype
from ProjetApp.views import Snp_search, Redirection_details
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

from ProjetApp.forms import SnpForm, ConnexionForm




urlpatterns = [
    path('', Home),
    path('admin/', admin.site.urls),
    path('connexion/', auth_views.login, {'template_name': 'T/connexion.html'}, name='login'),
    path('deconnexion/', auth_views.logout, {'next_page': 'home' }, name = 'logout'),
    path('signup/', sign_up, name='signup'),
    path('Home_page/', Home, name = "home"),
    path('SNP_search_form_page/', Snp_search, name = 'snp'),
    path('SNP_detail_page/<pk>/', Redirection_details, name = 'Redirection_details'),
    path('Phenotype_detail_page/<pk>/', PhenoDetails, name='phenoD'),
    path('Phenotype_list_page/', Phenotype, name = 'phenoL'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
